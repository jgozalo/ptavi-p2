import sys


class Calculadora():

    def __init__(self, op1, op2, operacion):
        """Esto es el método iniciliazador"""
        self.op1 = op1
        self.op2 = op2
        self.operacion = operacion

    def plus(self):
        """ Function to sum the operands. Ops have to be ints """
        return int(self.op1) + int(self.op2)

    def minus(self):
        """ Function to substract the operands """
        return int(self.op1) - int(self.op2)


if __name__ == "__main__":
    try:
        operando1 = int(sys.argv[1])
        operando2 = int(sys.argv[3])
        operacion = sys.argv[2]
    except ValueError:
        sys.exit("Error")

    objeto = Calculadora(operando1, operando2, operacion)
    if objeto.operacion == "suma":
        print(objeto.plus())
    elif objeto.operacion == "resta":
        print(objeto.plus())
    else:
        sys.exit('Operacion sólo puede ser sumar o restar.')
