from calcoohija import CalculadoraHija
import sys


class Calcplus(CalculadoraHija):
    pass


if __name__ == "__main__":
    fich = open(sys.argv[1])
    operaciones = fich.readlines()
    for operacion in operaciones:
        partes = operacion.split(",")
        operacion = partes[0]
        resultado = partes[1]
        objeto = Calcplus(resultado, 0, operacion)
        for num in partes[2:]:
            objeto.op2 = num
            resultado = objeto.seleccion()
            objeto.op1 = resultado
        print(int(resultado))
    fich.close()
