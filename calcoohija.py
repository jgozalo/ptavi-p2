from calcoo import Calculadora
import sys


class CalculadoraHija(Calculadora):

    def multiplica(self):
        """ Function to sum the operands. Ops have to be ints """
        return int(self.op1) * int(self.op2)

    def divide(self):
        """ Function to sum the operands. Ops have to be ints """
        return int(self.op1) / int(self.op2)

    def seleccion(self):
        if self.operacion == "multiplica":
            return self.multiplica()
        elif self.operacion == "divide":
            if self.op2 != 0:
                return self.divide()
            else:
                return "Division by zero is not allowed"
        elif self.operacion == "resta":
            return self.minus()
        elif self.operacion == "suma":
            return self.plus()
        else:
            sys.exit('Operación sólo puede ser sumar o restar.')


if __name__ == "__main__":
    try:
        operando1 = int(sys.argv[1])
        operando2 = int(sys.argv[3])
        operacion = sys.argv[2]
    except ValueError:
        sys.exit("Error")

    objeto = CalculadoraHija(operando1, operando2, operacion)
    print(objeto.seleccion())
