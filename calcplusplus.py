import csv
import sys
from calcplus import Calcplus


class calcCSV(Calcplus):
    pass


if __name__ == "__main__":
    with open(sys.argv[1], newline='') as File:
        reader = csv.reader(File)
        for row in reader:
            operacion = row[0]
            resultado = row[1]
            objeto = Calcplus(resultado, 0, operacion)
            for num in row[2:]:
                objeto.op2 = num
                resultado = objeto.seleccion()
                objeto.op1 = resultado
            print(int(resultado))
